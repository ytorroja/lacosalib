import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.geom.*;
import processing.opengl.*;
import saito.objloader.*;

Scene scene;

OBJModel model;
Panel3D_[] panels = null;

class Panel3D_ {

  Segment segment = null;
  Face[] faces = null;
  PShape panelShape = null;
  PShape[] sFaces = null;
  InteractiveFrame iFrame;
  Scene scene = null;

  color c;
  int alpha = 150;
  
  Panel3D_(Scene scene, Segment segment) {
    this.scene = scene;
    c = color(random(255),random(255),random(255),alpha);
    this.segment = segment;
    faces = segment.getFaces();
    sFaces = new PShape[faces.length];
    panelShape = createShape(GROUP);
    for(int i = 0; i < sFaces.length; i++){
      sFaces[i] = createShape();
      sFaces[i].beginShape(QUADS);
      PVector[] faceVertexs = faces[i].getVertices(); 
      for (int j = 0; j < faceVertexs.length; j++) {
        sFaces[i].vertex(faceVertexs[j].x, faceVertexs[j].y, faceVertexs[j].z);
      }
      sFaces[i].endShape();
      panelShape.addChild(sFaces[i]);
      panelShape.setFill(c);
    }   
    iFrame = new InteractiveFrame(scene, panelShape);
  }

  void draw() {
    if(iFrame.checkIfGrabsInput(mouseX, mouseY)){
      panelShape.setFill(color(255));
    } else {
      panelShape.setFill(c);
    }    
    shape(panelShape); 
  }
}


void setup() {

  size(1000, 800, P3D);

  scene = new Scene(this);
  scene.setRadius(700);
  scene.showAll();
  scene.eyeFrame().setDamping(0);
  scene.setPickingVisualHint(true);
  
  model = new OBJModel(this, "la_cosa_medialab.obj", "relative", QUADS);
  model.scale(70);
  model.translateToCenter();

  panels = new Panel3D_[model.getSegmentCount()];
  for (int i = 0; i < panels.length; i++) {
    panels[i] = new Panel3D_(scene, model.getSegment(i));
  }

}

void draw() {
  background(0);
  lights();
  noStroke();
  for (int i = 0; i < panels.length; i++) {
    if(panelCount == i){
      fill(255,0,0);
    } else{
      fill(100);
    }
    panels[i].draw();
  }
}

int panelCount = 0;
void keyReleased() {
  panelCount++;
  panelCount %= panels.length;
}

