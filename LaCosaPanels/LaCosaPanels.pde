String[] lines;
ArrayList<Panel> panels;

boolean debug = true;
boolean oneByOne = false;
boolean panelsOn = true;
int currentPanel;

void setup() {
  size(1024, 768, P2D);
  lines = loadStrings("CoordPaneles.txt");
  panels = new ArrayList<Panel>();
  for(int i = 0; i < lines.length; i++) {
    String parts[] = lines[i].split("\\.");
    String name = parts[0];
    int x = Integer.parseInt(parts[1]);
    int y = Integer.parseInt(parts[2]);
    int w = Integer.parseInt(parts[3]);
    int h = Integer.parseInt(parts[4]);
    panels.add( new Panel(name, x, y, w, h));
  }
}

void draw() {
  background(0);
  //fill(0,0,0,2);
  //rect(0,0,width,height);
  fill(255,0,0);
  for(Panel p : panels) {
    if (oneByOne) {
      if (panels.indexOf(p) == currentPanel) p.draw();
    } else {
      p.draw();
    }
  }
  printCornerInfo();
  
  if(frameCount % 1 == 0){
   //currentPanel = (currentPanel + 1) % panels.size(); 
  }
}

void keyPressed() {
  if (key == 'd' || key == 'D') // Change debug mode
    debug = ! debug;
  if (key == 'r' || key == 'R') // set random colors for all panels 
    for(Panel p : panels) p.setColor(color(random(255), random(255), random (255)));
  if (key == 'w' || key == 'W') // set white color for all panels
    for(Panel p : panels) p.setColor(0xFFFF0000);
  if (key == 'o' || key == 'O') {// set all panels on/off
    panelsOn = !panelsOn;
    for(Panel p : panels) p.setLight(panelsOn);
  }
  if (key == 'b' || key == 'B') // draw only panels boundaries (maight light on some leds!)
    for(Panel p : panels) p.setDrawBox(!p.getDrawBox());
  if (key == 's' || key == 'S')
    oneByOne = !oneByOne;
  if (oneByOne) {
    if (key == 'j' || key == 'J') 
      currentPanel = (currentPanel + panels.size() - 1) % panels.size();
    if (key == 'k' || key == 'K') 
      currentPanel = (currentPanel + 1) % panels.size();
  }
}

void printCornerInfo() {
  int x = 20;
  int y = 10;
  int inc = (int)(textAscent() + textDescent());
  text("(O) Panels are " + (panelsOn ? "ON" : "OFF"), x, y = y + inc);
  text("(D) Debug mode is " + (debug ? "ON" : "OFF"), x, y = y + inc);
  text("(S) Step mode is " + (oneByOne ? "ON" : "OFF"), x, y = y + inc);
  if (oneByOne) 
    text("(J, K) Current panel id is " + panels.get(currentPanel).getName(), x, y = y + inc);
  text("(W) Set all panels white", x, y = y + inc);
  text("(R) Set all panels random", x, y = y + inc);
  text("(B) Draw panel's external box (might light some leds!)", x, y = y + inc);
}