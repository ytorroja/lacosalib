class Panel {
  String name;
  int x, y;
  int w, h;
  int c;    // Color
  boolean drawBox;
  boolean light;
  
  Panel(String _name, int _x, int _y, int _w, int _h) {
    x = _x;
    y = _y;
    w = _w;
    h = _h;
    name = _name;
    c = color(random(255), random(255), random (255));
    light   = true;
    drawBox = false;
  }
  
  void setColor(int _c) {
    c = _c;
  }
  
  void draw() {
    pushMatrix();
    pushStyle();
    rectMode(CORNER);
    if (drawBox) {
      noFill();
      strokeWeight(1);
      stroke(255);
      rect(x-1, y-1, w+1, h+1); // Bounding box
    }
    if (light) {
      noStroke();
      fill(c);
      rect(x, y, w, h);
    }
    if (debug) {
      textMode(CENTER);
      translate(x + w/2, y + h/2);
      rotate(-PI/2);
      fill(255);
      text(name, 0, 0);  
    }    
    popStyle();
    popMatrix();
  }
  
  boolean getDrawBox() {
    return drawBox;
  }
  
  void setDrawBox(boolean _b) {
    drawBox = _b;
  }
  
  String getName() {
    return name;
  }
  
  void setLight(boolean _l) {
    light = _l;
  }

  boolean getLight() {
    return light;
  }
  
}
    
