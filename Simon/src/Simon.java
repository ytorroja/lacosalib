import java.util.ArrayList;
import java.util.List;

import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;

public class Simon extends PApplet {

	int MAX_COLORS = 3;
	int MAX_LENGTH = 40;

	int sequence[];

	int currentUserIdx;
	int currentShowIdx;
	int currentGameIdx;
	// int currentSeqIdx;

	int gameState = 0;
	int inSeqState = 0;
	long startSequence;
	long startTime;

	final int WAITING = 0;
	final int PRE_PLAYING = 1;
	final int PLAYING = 2;
	final int SHOW_SEQ = 3;
	final int WAIT_ANS = 4;
	final int WAIT_ANS_FINISH = 5;
	final int DYING = 6;

	final int STEP_DELAY_MS = 500;

	boolean state1 = false;
	boolean state2 = false;
	boolean state3 = false;
	boolean state4 = false;

	int timeOn = 200;
	int timeOnMoment = 0;

	OscP5 oscP5;
	NetAddress myRemoteLocation;
	NetAddress myRemoteLocationCosa;
	NetAddress myRemoteLocationAudio;

	public void setup() {
		size(300, 300);

		sequence = new int[MAX_LENGTH];
		for (int i = 0; i < MAX_LENGTH; i++) {
			sequence[i] = (int) random(MAX_COLORS);
		}

		oscP5 = new OscP5(this, 12000);

		myRemoteLocation = new NetAddress("127.0.0.1", 12002);
		myRemoteLocationCosa = new NetAddress("127.0.0.1", 12003);
		myRemoteLocationAudio = new NetAddress("127.0.0.1", 12004);

		endGame();
	}

	private void turnOff() {
		state1 = false;
		state2 = false;
		state3 = false;
		state4 = false;
	}

	public void draw() {
		background(0, 122);

		if (gameState == PLAYING) {
			//AQUÍ ESTAMOS JUGANDO
			if (inSeqState == SHOW_SEQ) {
				//MOSYRANDO SEQUENCIA
				if (millis() > startSequence + 1500)
					updateSequencePlay();
				else
					startTime = millis();
			} else if (inSeqState == WAIT_ANS) {
				//ESPERANDO RESPUESTA
				updateSequenceWaitAnswer();
			} else if (inSeqState == WAIT_ANS_FINISH) {
				//TIEMPO DE ESPERA ENTRE SECUENCIA INTRUDCIDA Y PASO A MOSTRAR LA SIGUEINTE
				updateSequenceWaitAnswer();
			}
		} else {
			//AQUÍ ESTAMOS EN INACTIVIDAD MANDANDO EVENTOS RANDOM
			if (frameCount % 90 == 0 && gameState == WAITING) {
				turnOn(0);
			}
			
			if (frameCount % 155 == 0 && gameState == WAITING) {
				turnOn(1);
			}
			
			if (frameCount % 120 == 0 && gameState == WAITING) {
				turnOn(2);
			}
		}

		drawTable();

		//TIMEOUT
		if (gameState == PLAYING && inSeqState == WAIT_ANS
				&& millis() > startTime + 30000) {
			endGame();
		}

		//DEBUG
		fill(255);
		for (int i = 0; i < currentGameIdx; i++)
			text(sequence[i], 20 * i, 250);
		text("User: " + currentUserIdx, 20, 280);
		text("Game: " + currentGameIdx, 80, 280);
		text("inSeqState: " + inSeqState, 180, 280);
		text("currentGameIdx: " + currentGameIdx, 180, 295);

	}

	private void updateSequenceWaitAnswer() {
		
		if (millis() > timeOnMoment) {
			turnOff();
		}

		if (inSeqState == WAIT_ANS_FINISH && millis() > timeOnMoment + 1000) {
			inSeqState = SHOW_SEQ;
			startSequence = millis();
			startTime = millis();
		}
	}

	public void sendStartGame() {
		OscMessage myMessage = new OscMessage("/simon");
		myMessage.add(1); /* add an int to the osc message */
		/* send the message */
		oscP5.send(myMessage, myRemoteLocation);
		oscP5.send(myMessage, myRemoteLocationCosa);
		oscP5.send(myMessage, myRemoteLocationAudio);
	}

	public void sendButton(int b) {
		if (gameState == PLAYING 
				|| gameState == WAITING) {
			println("encendemos " + b + "    " + millis());
			OscMessage myMessage = new OscMessage("/simon");
			myMessage.add(b); /* add an int to the osc message */
			/* send the message */
			oscP5.send(myMessage, myRemoteLocation);
			oscP5.send(myMessage, myRemoteLocationCosa);
			oscP5.send(myMessage, myRemoteLocationAudio);
		}
	}

	public void sendEndGame() {
		OscMessage myMessage = new OscMessage("/simon");
		myMessage.add(0); /* add an int to the osc message */
		/* send the message */
		oscP5.send(myMessage, myRemoteLocation);
		oscP5.send(myMessage, myRemoteLocationAudio);
		oscP5.send(myMessage, myRemoteLocationCosa);
	}

	public void updateSequencePlay() {

		if (millis() - startTime >= STEP_DELAY_MS * (currentShowIdx + 1)) {
			currentShowIdx++;
			turnOff();
			if (currentShowIdx >= currentGameIdx) {
				inSeqState = WAIT_ANS;
				// gameState = WAITING;
				currentShowIdx = 0;
				currentUserIdx = 0;
				println("currentGameIdx " + currentGameIdx);
				turnOff();
			}
		} else {
			// println("no se que estoy haciendo");
		}

		if (inSeqState == SHOW_SEQ)
			if ((millis() - startTime) % STEP_DELAY_MS > STEP_DELAY_MS / 2) {
				turnOff();
			} else {
				turnOn(sequence[currentShowIdx]);
			}
	}

	public void oscEvent(OscMessage theOscMessage) {
		/* print the address pattern and the typetag of the received OscMessage */
		print("### received an osc message.");
		print(" addrpattern: " + theOscMessage.addrPattern());
		println(" typetag: " + theOscMessage.typetag());
		int b = theOscMessage.get(0).intValue();

		if (b == 1) {
			checkKey('1');
		}
		
		if (b == 2) {
			if (gameState == WAITING) {
				gameState = PRE_PLAYING;
				sendStartGame();
			} else {
				checkKey('2');
			}
		}
		
		if (b == 3) {
			checkKey('3');
		}
		
		if (b == 4) {
			println("Received end of scale up");
			initGame();
		}


	}

	private void turnOn(int i) {
		// println(" encender ese " + i);
		switch (i) {
		case 0:
			if (gameState == WAITING || !state1)
				sendButton(10);
			state1 = true;
			break;
		case 1:
			if (gameState == WAITING || !state2)
				sendButton(20);
			state2 = true;

			break;
		case 2:
			if (gameState == WAITING ||!state3)
				sendButton(30);
			state3 = true;
			break;
		case 3:
			state4 = true;
			break;

		default:
			println("no tenemos pa encender ese " + i);
			break;
		}
	}

	public void drawTable() {
		rectMode(CENTER);

		fill(255, 0, 0, (state1 ? 255 : 80));
		rect(100, 100, 100, 100);
		fill(255);
		text("1", 100, 100);

		fill(0, 0, 255, (state2 ? 255 : 80));
		rect(200, 100, 100, 100);
		fill(255);
		text("2", 200, 100);

		fill(0, 255, 0, (state3 ? 255 : 80));
		rect(100, 200, 100, 100);
		fill(255);
		text("3", 100, 200);

		if (MAX_COLORS > 3) {
			fill(0, 255, 255, (state4 ? 255 : 80));
			rect(200, 200, 100, 100);
			fill(255);
			text("4", 200, 200);
		}
		// println("drawing ");
	}

	public void checkKey(int key) {
		if (gameState == WAITING) return;
		timeOnMoment = millis() + timeOn;
		turnOn(key - '0' - 1);
		if (key - '0' - 1 == sequence[currentUserIdx]) {
			println("Acertaste en paso " + currentUserIdx);
			if (currentUserIdx >= currentGameIdx - 1) {
				currentGameIdx++;
				currentUserIdx = 0;
				currentShowIdx = 0;
				startTime = millis();
				inSeqState = WAIT_ANS_FINISH;
			} else {
				currentUserIdx++;
			}
		} else {
			endGame();
		}
	}

	private void endGame() {
		turnOff();
		sendEndGame();
		currentUserIdx = 0;
		currentGameIdx = 0;
		gameState = WAITING;
	}

	public void keyPressed() {
		switch (gameState) {
		case WAITING:
			if (key == '0') {
				gameState = PRE_PLAYING;
				sendStartGame();
			}
			break;
		default:
			checkKey(key);
			break;
		}
	}

	private void initGame() {
		gameState = PLAYING;
		currentGameIdx = 1;
		inSeqState = SHOW_SEQ;
		startTime = millis();
		startSequence = millis();
		turnOff();
		// sendStartGame();
		for (int i = 0; i < MAX_LENGTH; i++) {
			sequence[i] = (int) random(MAX_COLORS);
		}
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "Simon" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}

}
