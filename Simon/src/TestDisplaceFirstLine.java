import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;
import ddf.minim.analysis.FFT;
import ddf.minim.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import org.mlp.cosa.Cosa;
import org.mlp.cosa.Panels;

public class TestDisplaceFirstLine extends PApplet {

	Cosa cosa;

	PanelRender render;

	int state = 0;

	int STATE_ALL = 0;
	int STATE_FLOOR1 = 1;
	int STATE_FLOOR2 = 1;

	Panels floor1Der = null;
	Panels floor2 = null;
	Panels floor1Izq = null;

	public void setup() {
		size(1024, 768);
		
		frameRate(24);

		cosa = new Cosa();
		cosa = new Cosa();
		File pathP = sketchFile("paneles.txt");
		File pathC = sketchFile("CoordCeldas.txt");
		cosa.init(pathP, pathC);

		render = new PanelRender();

		floor1Der = cosa.getByTag("floor1");
		floor2 = cosa.getByTag("floor2");
		floor1Izq = cosa.getByTag("floor1i");

		floor1Der.setColor(0xffffff00);
		floor2.setColor(0xff00ffff);
		floor1Izq.setColor(0xffff00ff);
		// floor3.setColor(0xff0000ff);

	}

	public void draw() {
		// background(0);
		float factor = ((exp(sin(millis()/1000.0f*PI)) - 0.36787944f)*108f);

		factor = map(factor,0.0f,255.0f,255.0f,0.0f);
		
		cosa.getByTag("floor1").setColor(0xffff00ff);
		cosa.getByTag("floor2").setColor(0xffff00ff);
		cosa.getByTag("floor1i").setColor(0xffff00ff);
		

		if (state == 0)
			render.render(cosa, g);

		if (state == 1) {
			 cosa.setColor(0xffffffff);
			render.render(cosa, g);
		}

		if (state == 2)
			render.render(floor1Der, g);

		if (state == 3)
			render.render(floor2, g);

		if (state == 4)
			render.render(floor1Izq, g);

		
		fill(0,0,0, factor);
		rect(0, 0, width, height);

	}

	public void keyPressed() {
		int tempstate = key - '0';
		if(tempstate >= 0 && tempstate<=4){
			state = tempstate;
		}
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "--present",
				"--window-color=#666666", "--hide-stop",
				"TestDisplaceFirstLine" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}
