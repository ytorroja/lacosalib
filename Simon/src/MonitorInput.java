import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;
import ddf.minim.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import codeanticode.syphon.SyphonServer;

public class MonitorInput extends PApplet {

	/**
	 * This sketch demonstrates how to monitor the currently active audio input
	 * of the computer using an AudioInput. What you will actually be monitoring
	 * depends on the current settings of the machine the sketch is running on.
	 * Typically, you will be monitoring the built-in microphone, but if running
	 * on a desktop it's feasible that the user may have the actual audio output
	 * of the computer as the active audio input, or something else entirely.
	 * <p>
	 * Press 'm' to toggle monitoring on and off.
	 * <p>
	 * When you run your sketch as an applet you will need to sign it in order
	 * to get an input.
	 * <p>
	 * For more information about Minim and additional features, visit
	 * http://code.compartmental.net/minim/
	 */

	Minim minim;
	AudioInput in;

	float[] l = new float[1024];
	float[] r = new float[1024];

	float[] l1 = new float[1024];
	float[] r1 = new float[1024];

	float[] l2 = new float[1024];
	float[] r2 = new float[1024];
	SyphonServer server;

	public void setup() {
		size(1024, 768, OPENGL);
		server = new SyphonServer(this, "Processing Syphon");
		minim = new Minim(this);

		// use the getLineIn method of the Minim object to get an AudioInput
		in = minim.getLineIn();
	}

	public void draw() {
		background(0);
		stroke(255);

		for (int i = 0; i < in.bufferSize() - 1; i++) {
			l[i] = in.left.get(i);
			r[i] = in.left.get(i);
		}

		// if (frameCount % 16 == 0)
		for (int i = 0; i < in.bufferSize() - 1; i++) {
			l1[i] = l1[i] + (l[i] - l1[i]) * 0.1f;
			r1[i] = in.right.get(i);
		}

		for (int i = 0; i < in.bufferSize() - 1; i++) {
			l2[i] = in.left.get(i);
			r2[i] = in.right.get(i);
		}
		float energy = 0;

		l2[0] = l2[0] + (energy - l2[0]) * 0.001f;
		float e2 = l2[0];

		for (int i = 0; i < in.bufferSize() - 1; i++) {
			// line(i, offline1 + l[i] * scale, i + 1, offline1 + l[i + 1]
			// * scale);
			if (l[i] < 0)
				energy += -l1[i];
			else
				energy += l1[i];
		}

		energy = energy / in.bufferSize();

		translate(00, 300);
		float scale = 1500;
		if (frameCount % 60 > 30) {
			// scale*=-1;
		}
		for (int i = 0; i < in.bufferSize() - 1; i++) {
			// stroke(e2);
			pushMatrix();
			strokeWeight(1);
			if (i % 3 == 0) {
				stroke(255);

				// line(i, 50 + l1[i] * scale, i + 1, 50 + l1[i + 1] * scale *
				// e2);
				for (int j = 0; j < 1; j++) {
					pushMatrix();

					translate(i, j * 50);
					line(0, 50 + l1[i] * scale, 0 + 1, 50 + l1[0 + 1] * scale
							* e2);

					rotate(e2 * frameCount);
					stroke(255, 0, 255);
					strokeWeight(150 * e2);
					rect(0 * 3, 50 + l1[i] * scale, 0 - 1, 1 * e2 * 10);
					popMatrix();
				}
				fill(255);
				stroke(255);
				// rotate(energy*0.10f);
				// rect(i, 50 + l1[i] * scale * e2, 1, 1 * e2 * 2550);
			}
			popMatrix();
		}

		// stroke(255,0,0);
		// strokeWeight(10);
		//
		// ellipse(mouseX, mouseY, 10, 100);
		// translate(0, 150);
		//
		// float energy = 0;
		//
		// stroke(255);
		// pushMatrix();
		// // draw the waveforms so we can see what we are monitoring
		// for (int j = 0; j < 1; j++) {
		// for (int i = 0; i < in.bufferSize() - 1; i++) {
		// // line(i, offline1 + l[i] * scale, i + 1, offline1 + l[i + 1]
		// // * scale);
		// if (l[i] < 0)
		// energy += -l[i];
		// else
		// energy += l[i];
		// }
		// energy = energy / in.bufferSize();
		translate(-100, 0);
		// }
		// popMatrix();
		// // energy *= 50;
		if (frameCount % 60 > 25) {
			fill(0,255,255);
			rect(500,0,50+e2*100,height);
		}else{
			rect(width/2+00,100,width*2,50+e2*1000);
		}
		stroke(255);
		strokeWeight(2);
		noFill();
		for (int i = 0; i < 100; i++) {
			ellipse(600 + 60 * e2 * (random(150) - 2), 50 + 90 * e2
					* (random(15) - 2), 200+i*2, 200+i*2);
		}

		// }
		//
		// if (frameCount % 120 > 60) {
		// // noFill();
		// background(0);
		// stroke(255, 255, 0);
		// strokeWeight(15);
		// for (int i = 0; i < 50; i++) {
		// noFill();
		// ellipse(width / 2 + e2 * 10 - 5, height / 2 - 300 + e2 * 10
		// - 5, 900 * e2, 900 * e2);
		// }
		// }
		//
		// if (frameCount % 120 > 90) {
		// // noFill();
		// background(0);
		// stroke(255, 255, 0);
		// strokeWeight(15);
		// for (int i = 0; i < 50; i++) {
		// noFill();
		// if (frameCount % 2 == 0){
		// rectMode(CENTER);
		// rotate(e2);
		// }
		// rect(width / 2 + e2 * 10 - 5, height / 2 - 300 + e2 * 10
		// - 5, 900 * e2, 900 * e2);
		// }
		// }
		//
		// }
		// } else {
		// // noFill();
		// //energy /= 50;
		// fill(energy, 0, energy * 255);
		// rectMode(CENTER);
		// // rect(width/2,height/2-300,9*energy,9*energy);
		// rect(width / 2, height / 2 - 300, 900 * energy, 900 * energy);
		// }
		// }
		//
		// translate(520, 350);
		// strokeWeight(2);
		//
		// float offline1 = 500;
		// float scale = 1;
		// float offline2 = 550;
		// noFill();
		// // println(energy);
		// for (int j = 0; j < 1; j++) {
		// for (int i = 0; i < in.bufferSize() / 2 - 1; i++) {
		// rotate(2 * PI / 152*energy);
		//
		// line(0, offline1 + l[i ] * energy, -i ,
		// offline1 + l[i + 1] * energy - 150 );
		//
		// }
		// translate(-10, 20);
		// }

		server.sendScreen();
	}

	public void settings() {
		size(512, 200, P3D);
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "MonitorInput" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}
