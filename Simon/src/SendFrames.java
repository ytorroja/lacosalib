import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;

import codeanticode.syphon.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

public class SendFrames extends PApplet {

	PGraphics canvas;
	SyphonServer server;

	public void setup() {
		size(1024, 768, P2D);
		canvas = createGraphics(1024, 768, P3D);

		// Create syhpon server to send frames out.
		server = new SyphonServer(this, "Processing Syphon");
	}

	
	
	public void draw() {
		canvas.beginDraw();
		canvas.background(0);
		canvas.stroke(255);
		canvas.strokeWeight(3);
		canvas.noFill();
		//canvas.translate(frameCount%width/20.0f,0);
		canvas.translate(0,canvas.height);
		for(int i = 0;i<180;i++){
			
			//canvas.line(0,0,0,canvas.height+mouseX);	
			float cc = 1000-frameCount%500;
			//cc*=20*sin(frameCount/100.0f);
			//cc=10;
			canvas.ellipse(0,0,cc*i/10f,cc*i/10f);
			
			//canvas.rotate(frameCount/60000.0f);
			if(i%5==0)
			canvas.stroke(255,255,0);
			else
				canvas.stroke(255);
				
		}
		
		canvas.endDraw();
		image(canvas, 0, 0);
		server.sendImage(canvas);
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "SendFrames" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}
