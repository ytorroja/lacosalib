import java.util.ArrayList;
import java.util.List;

import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;
import processing.core.PVector;

public class SimonInterfaz extends PApplet {

	PVector b1 = new PVector();
	PVector b2 = new PVector();
	PVector b3 = new PVector();

	float radio = 75;

	OscP5 oscP5;
	NetAddress myRemoteLocation;

	public void setup() {
		size(300, 300);
		oscP5 = new OscP5(this, 12001);
		myRemoteLocation = new NetAddress("127.0.0.1", 12000);

		b1.x = width / 4;
		b2.x = width * 2 / 4;
		b3.x = width * 3 / 4;
		
		b1.y = height / 2;
		b2.y = height / 2;
		b3.y = height / 2;
		
	}

	public void draw() {
		background(0, 122);

		fill(255, 0, 0);
		ellipse(b1.x, b1.y, radio, radio);
		fill(0, 0, 255);
		ellipse(b2.x, b2.y, radio, radio);
		fill(0, 255, 0);
		
		ellipse(b3.x, b3.y, radio, radio);

	}

	@Override
	public void mouseClicked() {
		float dist = dist(b1.x, b1.y, mouseX, mouseY);
		if (dist < radio/2) {
			println("PULSAMOS BOTON 1");
			sendButton(1);
		}

		dist = dist(b2.x, b2.y, mouseX, mouseY);
		if (dist < radio/2) {
			println("PULSAMOS BOTON 2");
			sendButton(2);
		}

		dist = dist(b3.x, b3.y, mouseX, mouseY);
		if (dist < radio/2) {
			println("PULSAMOS BOTON 3");
			sendButton(3);
		}
	}
	
	public void sendButton(int numB){
		 OscMessage myMessage = new OscMessage("/buttonPressed");
		  
		  myMessage.add(numB); /* add an int to the osc message */

		  /* send the message */
		  oscP5.send(myMessage, myRemoteLocation); 
	}

	public void oscEvent(OscMessage theOscMessage) {
		/* print the address pattern and the typetag of the received OscMessage */
		
	}
	
	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "SimonInterfaz" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}

}
