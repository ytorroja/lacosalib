import java.util.List;

import org.mlp.cosa.Cell;
import org.mlp.cosa.Cosa;
import org.mlp.cosa.Panel;
import org.mlp.cosa.Panels;
import org.mlp.cosa.Point;

import processing.core.PApplet;
import processing.core.PGraphics;

public class CellRender {

	public void render(Cosa cosa, PGraphics canvas) {

		for (Panel p : cosa.getPanels()) {
			draw(canvas, p);
		}
	}

	public void render(Panels panels, PGraphics canvas) {

		for (Panel p : panels) {
			draw(canvas, p);
		}
	}

	public void draw(PGraphics canvas, Panel panel) {
		canvas.pushMatrix();
		canvas.pushStyle();
		canvas.rectMode(PApplet.CORNER);

		List<Cell> cells = panel.getCells();

		for (Cell cell : cells) {

			if (true) {
				float rt = canvas.red(cell.colorTarget);
				float gt = canvas.green(cell.colorTarget);
				float bt = canvas.blue(cell.colorTarget);

				float r = canvas.red(cell.color);
				float g = canvas.green(cell.color);
				float b = canvas.blue(cell.color);

				float vel = 0.05f;

				r = r + (rt - r) * vel;
				g = g + (gt - g) * vel;
				b = b + (bt - b) * vel;
				// PApplet.println(rt + " " + gt + " " + bt);
				cell.color = canvas.color(r, g, b);
			}

			if (panel.isDraw) {
				canvas.noFill();
				canvas.strokeWeight(1);
				canvas.stroke(255);
				canvas.rect(panel.x - 1, panel.y - 1, panel.width + 1,
						panel.height + 1); // Bounding box
			}
			if (panel.light) {
				canvas.noStroke();
				canvas.fill(panel.color);
				canvas.rect(panel.x, panel.y, panel.width, panel.height);
			}
			if (panel.debug) {
				canvas.textMode(PApplet.CENTER);
				canvas.translate(panel.x + panel.width / 2, panel.y
						+ panel.height / 2);
				canvas.rotate(-PApplet.PI / 2);
				canvas.fill(255);
				canvas.text(panel.name, 0, 0);
			}
			canvas.popStyle();
			canvas.popMatrix();
		}
	}

	void draw(PGraphics canvas, Cell cell) {
		canvas.pushStyle();
		canvas.strokeWeight(1);
		canvas.stroke(cell.color);
		// if (!finished) stroke(255);
		canvas.beginShape();
		for (Point p : cell.polygon)
			canvas.vertex(p.x, p.y);
		canvas.endShape(PApplet.CLOSE);
		canvas.popStyle();
	}
}
