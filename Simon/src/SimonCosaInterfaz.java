import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;
import ddf.minim.analysis.FFT;
import ddf.minim.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import org.mlp.cosa.Cosa;
import org.mlp.cosa.Panel;
import org.mlp.cosa.Panels;

import oscP5.*;
import netP5.*;

public class SimonCosaInterfaz extends PApplet {

	Cosa cosa;

	PanelRender render;

	int lastAction = 0;

	int stateGame = 0;

	int stateReposo = 1;

	int STATE_ALL = 0;
	int STATE_FLOOR1 = 1;
	int STATE_FLOOR2 = 1;

	Panels floor1Der = null;
	Panels floor2 = null;
	Panels floor1Izq = null;

	OscP5 oscP5;
	NetAddress myRemoteLocation;

	int color = 255;

	int velocidad = 0;

	// secuencia cero reposo
	int colores[] = new int[3];
	int indiceColor = 0;

	// secuencia 1 de reposo

	int indicePanel = 0;

	// secuencia4, encendido todos desde sufhle
	List<Panel> shufflePanels = new ArrayList<Panel>();
	int shuffleState = 0;

	int SMOOTH_STATE = 1;

	public void setup() {
		size(1024, 768);

		frameRate(60);

		File file = dataFile("paneles.txt");

		cosa = new Cosa();
		cosa = new Cosa();
		File pathP = sketchFile("paneles.txt");
		File pathC = sketchFile("CoordCeldas.txt");
		cosa.init(pathP, pathC);

		shufflePanels = new ArrayList(cosa.getPanels());
		Collections.shuffle(shufflePanels);

		render = new PanelRender();

		floor1Der = cosa.getByTag("floor1");
		floor2 = cosa.getByTag("floor2");
		floor1Izq = cosa.getByTag("floor1i");

		floor1Der.setColor(0xffffff00);
		floor2.setColor(0xff00ffff);
		floor1Izq.setColor(0xffff00ff);
		// floor3.setColor(0xff0000ff);

		oscP5 = new OscP5(this, 12003);
		myRemoteLocation = new NetAddress("127.0.0.1", 12000);

		colores[0] = color(255, 0, 0);
		colores[1] = color(0, 255, 0);
		colores[2] = color(0, 0, 255);

	}

	public void draw() {

		float factor = (sin(frameCount * 0.5f) + 1) / 2;

		factor = ((exp(sin(millis() / 2000.0f * PI)) - 0.36787944f) * 108f);
		factor = map(factor, 0.0f, 255.0f, 255.0f, 0.0f);
		// factor *=factor;
		// println(factor);
		factor = 255;

		if (stateGame == 1) {
			background(0);
			floor1Der.setColor(color(color, 0, 0));
			floor2.setColor(color(0, 0, color));
			floor1Izq.setColor(color(0, color, 0));

			if (lastAction == 0)
				render.render(cosa, g);

			if (lastAction == 1) {
				cosa.setColor(0xffffffff);
				render.render(cosa, g);
			}

			if (lastAction == 2)
				render.render(floor1Der, g);

			if (lastAction == 3)
				render.render(floor2, g);

			if (lastAction == 4)
				render.render(floor1Izq, g);

			color = color + velocidad;
			if (color <= 0) {
				if (stateGame == 1) {
					// velocidad = 0;
					color = 0;
				} else {
					// color = 255;
				}
			}

		} else {

			if (stateReposo == 0) {
				fill(0, 10);
				rect(0, 0, width, height);
				cosa.setState(0);
				floor1Der.setColor(colores[indiceColor % 3]);
				floor2.setColor(colores[(indiceColor + 1) % 3]);
				floor1Izq.setColor(colores[(indiceColor + 1 + 1) % 3]);

				render.render(floor1Der, g);
				render.render(floor2, g);
				render.render(floor1Izq, g);

			} else if (stateReposo == 1) {
				// latido
				cosa.setState(0);
				fill(0, 2);
				rect(0, 0, width, height);

				Panels panels = cosa.getPanels();
				// if (frameCount % 1 == 0) {
				panels.setColor(0);
				indicePanel++;
				if (indicePanel >= panels.size()) {
					indicePanel = 0;
				}
				panels.get(indicePanel).setColor(color(255, 0, 0));
				// }

				render.draw(g, panels.get(indicePanel));
			} else if (stateReposo == 2) {
				// encendemos los últimos botones
				fill(0, 5);
				rect(0, 0, width, height);

				floor1Der.setColor(color(color, 0, 0));
				floor2.setColor(color(0, 0, color));
				floor1Izq.setColor(color(0, color, 0));

				if (lastAction == 2)
					render.render(floor1Der, g);

				if (lastAction == 3)
					render.render(floor2, g);

				if (lastAction == 4)
					render.render(floor1Izq, g);
			} else if (stateReposo == 3) {
				// recorremos todos a random
				cosa.setState(0);
				fill(0, 2);
				rect(0, 0, width, height);

				Panels panels = cosa.getPanels();
				if (frameCount % 8 == 0) {
					panels.setColor(0);
					indicePanel++;
					indiceColor++;
					if (indicePanel >= panels.size()) {
						indicePanel = 0;
					}
					panels.get(indicePanel).setColor(colores[indiceColor % 3]);
				}

				render.draw(g, panels.get(indicePanel));
			} else if (stateReposo == 4) {
				// shuffle de llenado y apagamos
				fill(0, 5);
				rect(0, 0, width, height);
				cosa.setState(SMOOTH_STATE);
				Panels panels = cosa.getPanels();
				if (frameCount % 4 == 0) {
					// lo hacemos ne todos los frames
					indicePanel++;
					if (indicePanel >= shufflePanels.size()) {
						// cuando llegamos al final todo a negro y volvemos a
						// empezar
						indicePanel = 0;
						panels.setColor(0);
						shufflePanels = new ArrayList(panels);
						Collections.shuffle(shufflePanels);
						delay(1300);
						shuffleState++;
						if (shuffleState > 3) {
							shuffleState = 0;
						}
					}

					if (shuffleState == 0) {
						shufflePanels.get(indicePanel).setColor(
								color(random(255), 100 + random(255),
										random(255)));
					} else if (shuffleState == 1) {
						shufflePanels.get(indicePanel).setColor(
								color(255, 0, 0));
					} else if (shuffleState == 2) {
						shufflePanels.get(indicePanel).setColor(
								color(0, 255, 0));
					} else if (shuffleState == 3) {
						shufflePanels.get(indicePanel).setColor(
								color(0, 0, 255));
					}
				}
				render.render(panels, g);
			} else if (stateReposo == 5) {
				// latido
				cosa.setState(0);
				fill(0, 2);
				rect(0, 0, width, height);

				Panels panels = cosa.getPanels();
				// if (frameCount % 1 == 0) {
				panels.setColor(0);
				indicePanel++;
				if (indicePanel >= panels.size()) {
					indicePanel = 0;
				}
				panels.get(indicePanel).setColor(color(255, 255, 0));
				// }

				render.draw(g, panels.get(indicePanel));
			} else if (stateReposo == 6) {
				// latido
				cosa.setState(0);
				fill(0, 2);
				rect(0, 0, width, height);

				Panels panels = cosa.getPanels();
				// if (frameCount % 1 == 0) {
				panels.setColor(0);
				indicePanel++;
				if (indicePanel >= panels.size()) {
					indicePanel = 0;
				}
				panels.get(indicePanel).setColor(color(255, 255, 255));
				// }

				render.draw(g, panels.get(indicePanel));
			} else if (stateReposo == 7) {
				// latido
				cosa.setState(0);
				fill(0, 2);
				rect(0, 0, width, height);

				Panels panels = cosa.getPanels();
				// if (frameCount % 1 == 0) {
				panels.setColor(0);
				indicePanel++;
				if (indicePanel >= panels.size()) {
					indicePanel = 0;
				}
				panels.get(indicePanel).setColor(color(255, 0, 255));
				// }

				render.draw(g, panels.get(indicePanel));
			} else if (stateReposo == 8) {
				// latido
				cosa.setState(0);
				fill(0, 2);
				rect(0, 0, width, height);

				Panels panels = cosa.getPanels();
				// if (frameCount % 1 == 0) {
				panels.setColor(0);
				indicePanel++;
				if (indicePanel >= panels.size()) {
					indicePanel = 0;
				}
				panels.get(indicePanel).setColor(color(0, 0, 255));
				// }

				render.draw(g, panels.get(indicePanel));
			}
			if (frameCount % 520 == 0) {
				stateReposo++;
				stateReposo = stateReposo % 9;
				indicePanel = 0;
				// stateReposo = 4;
			}
		}

		fill(255);
		text("color:" + color, 10, 10);
		text("stateReposo:" + stateReposo, 50, 10);

	}

	public void oscEvent(OscMessage theOscMessage) {
		/* print the address pattern and the typetag of the received OscMessage */
		int msg = theOscMessage.get(0).intValue();

		// evento de inicio de partida
		if (msg == 1) {
			color = 255;
			stateGame = 1;
		}

		// evento de fin de partida
		if (msg == 0) {
			color = 0;
			stateGame = 0;
			velocidad = -8;
		}
		if (msg == 10) {
			println("pintamos 10");
			lastAction = 2;
			velocidad = -16;
			color = 255;
		}

		if (stateGame == 0) {
			if (msg == 10 || msg == 20 || msg == 30) {
				indiceColor++;
				if (indiceColor > 2) {
					indiceColor = 0;
				}

			}
		}

		if (msg == 20) {
			println("pintamos 20");
			lastAction = 3;

			if (lastAction == 1)
				velocidad = -16;
			color = 255;
		}

		if (msg == 30) {
			println("pintamos 30");
			lastAction = 4;
			velocidad = -16;
			color = 255;
		}

	}

	public void keyPressed() {
		if (key > '0' && key < '9')
			lastAction = key - '0';
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "--present",
				"--window-color=#666666", "--hide-stop", "SimonCosaInterfaz" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}
