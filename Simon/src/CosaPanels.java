import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;
import ddf.minim.analysis.FFT;
import ddf.minim.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import org.mlp.cosa.Cosa;
import org.mlp.cosa.Panel;
import org.mlp.cosa.Panels;

import oscP5.*;
import netP5.*;

public class CosaPanels extends PApplet {

	Cosa cosa;

	PanelRender render;

	private int indicePanel;

	public void setup() {
		size(1024, 768);

		frameRate(60);

		cosa = new Cosa();
		cosa = new Cosa();
		File pathP = sketchFile("paneles.txt");
		File pathC = sketchFile("CoordCeldas.txt");
		cosa.init(pathP, pathC);

		render = new PanelRender();

	}

	public void draw() {

		//background(100);
		fill(0,5);
		rect(0,0,width,height);
		cosa.setState(0);

		Panels panels = cosa.getPanels();
		if (frameCount % 2 == 0) {
			cosa.setColor(0);
			panels.get(indicePanel).light = false;
			indicePanel++;
			if (indicePanel >= panels.size()) {
				indicePanel = 0;
			}
			panels.get(indicePanel).light = true;
			panels.get(indicePanel).setColor(color(255, 0, 0));
		}
		render.renderPanels(cosa, g);
	}

	public void keyPressed() {
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "--present",
				"--window-color=#666666", "--hide-stop", "CosaPanels" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}
