/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital
  pin 13, when pressing a pushbutton attached to pin 2.


  The circuit:
   LED attached from pin 13 to ground
   pushbutton attached to pin 2 from +5V
   10K resistor attached to pin 2 from ground

   Note: on most Arduinos there is already an LED on the board
  attached to pin 13.


  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Button
*/



#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            7
#define PIN2           2
#define PIN3           3

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      14

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels2 = Adafruit_NeoPixel(NUMPIXELS, PIN2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels3 = Adafruit_NeoPixel(NUMPIXELS, PIN3, NEO_GRB + NEO_KHZ800);

// constants won't change. They're used here to
// set pin numbers:
const int buttonPin1 = 4;     // the number of the pushbutton pin
const int buttonPin2 = 5;     // the number of the pushbutton pin
const int buttonPin3 = 6;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

int color1 = 0;
int color2 = 0;
int color3 = 0;

int lastb1Push = 0;
int lastb2Push  = 0;
int lastb3Push  = 0;

int timeAntirebote = 500;

// variables will change:
int buttonState1 = 0;         // variable for reading the pushbutton status
int buttonState2 = 0;         // variable for reading the pushbutton status
int buttonState3 = 0;         // variable for reading the pushbutton status

int buttonLastState1 = 0;         // variable for reading the pushbutton status
int buttonLastState2 = 0;         // variable for reading the pushbutton status
int buttonLastState3 = 0;
int delayval = 500; // delay for half a second

int state = 1;

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);

  // initialize the pushbutton pin as an input:
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT);

#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

  pixels.begin(); // This initializes the NeoPixel library.
  pixels2.begin(); // This initializes the NeoPixel library.
  pixels3.begin(); // This initializes the NeoPixel library.

  Serial.begin(9600);
}

void loop() {
  // read the state of the pushbutton value:
  buttonLastState1 = buttonState1;
  buttonLastState2 = buttonState2;
  buttonLastState3 = buttonState3;

  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);

  //println(buttonState);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState1 == HIGH || buttonState2 == HIGH || buttonState3 == HIGH) {
    //if (buttonState1 == HIGH || buttonState2 == HIGH) {
    // turn LED on:
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    // turn LED off:
    digitalWrite(LED_BUILTIN, LOW);
  }

  if (buttonState1 == HIGH && buttonLastState1 == LOW) {
    color1 = 255;
    // if(millis() > lastb1Push){
    lastb1Push = millis() + timeAntirebote;
    Serial.println('1');
    //}
  }

  if (buttonState2 == HIGH && buttonLastState2 == LOW) {
    color2 = 255;
    // if(millis() > lastb2Push){
    lastb2Push = millis() + timeAntirebote;
    Serial.println('2');
    //}
  }

  if (buttonState3 == HIGH && buttonLastState3 == LOW) {
    color3 = 255;
    //if(millis() > lastb3Push){
    lastb3Push = millis() + timeAntirebote;
    Serial.println('3');
    //}
  }

  if (Serial.available() > 0) {
    int msg = Serial.read();
    //PASAMOS A ESTADO DE JUEGO
    if (msg == 9) {
      state = 1;
      color1 = 0;
      color2 = 0;
      color3 = 0;
    }
    //PASAMOS A ESTADO DE REPOSO
    if (msg == 0) {
      state = 0;
      color2 = 255;
      color1 = 255;
      color3 = 255;
    }

    if (msg == 1) {
      color1 = 255;
    }
    if (msg == 2) {
      color2 = 255;
    }

    if (msg == 3) {
      color3 = 255;
    }
  }

  int vel = 7;
  //apagado smooth
  if (state == 0) {
    vel = 2;
  }

  if (color1 > 0)
    color1 -= vel;
  if (color2 > 0)
    color2 -= vel;
  if (color3 > 0)
    color3 -= vel;

  if (color1 < 0) {
    color1 = 0;
  }
  if (color2 < 0) {
    color2 = 0;
  }
  if (color3 < 0) {
    color3 = 0;
  }

  if (state == 0) {
    if (color2 == 0) {
      //color2 = 255;
    }
  }

  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(color1, 0, 0)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.

    pixels2.setPixelColor(i, pixels2.Color(0,0,color2)); // Moderately bright green color.
    pixels2.show(); // This sends the updated pixel color to the hardware.

    pixels3.setPixelColor(i, pixels3.Color(0, color3, 0)); // Moderately bright green color.
    pixels3.show(); // This sends the updated pixel color to the hardware.
  }

  
}
