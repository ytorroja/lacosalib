/**
 * Serial Duplex 
 * by Tom Igoe. 
 * 
 * Sends a byte out the serial port when you type a key
 * listens for bytes received, and displays their value. 
 * This is just a quick application for testing serial data
 * in both directions. 
 */


import processing.serial.*;
import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;
Serial myPort;      // The serial port
int whichKey = -1;  // Variable to hold keystoke values
int inByte = -1;    // Incoming serial data

int antireboteTime = 250;
int antireboteMoment = 0;

void setup() {
  size(400, 300);
  // create a font with the third font available to the system:
  PFont myFont = createFont(PFont.list()[2], 14);
  textFont(myFont);

  // List all the available serial ports:
  printArray(Serial.list());

  // I know that the first port in the serial list on my mac
  // is always my  FTDI adaptor, so I open Serial.list()[0].
  // In Windows, this usually opens COM1.
  // Open whatever port is the one you're using.
  String portName = Serial.list()[32];
  myPort = new Serial(this, portName, 9600);

  oscP5 = new OscP5(this, 12002);
  myRemoteLocation = new NetAddress("127.0.0.1", 12000);
}

void draw() {
  background(0);
  text("Last Received: " + inByte, 10, 130);
  text("Last Sent: " + whichKey, 10, 100);
}

public void sendButton(int numB){
     OscMessage myMessage = new OscMessage("/buttonPressed");
      
      myMessage.add(numB); /* add an int to the osc message */

      /* send the message */
      oscP5.send(myMessage, myRemoteLocation); 
  }

void serialEvent(Serial myPort) {
  inByte = myPort.read();
  if (inByte != 13 && inByte != 10)
    
  if (millis() > antireboteMoment) {
    println(inByte-'0');
  
    antireboteMoment = millis() +antireboteTime;
    if (inByte-'0' == 1) {
      sendButton(1);
    }
    if (inByte-'0' == 2) {
      sendButton(2);
    }
    if (inByte-'0' == 3) {
     sendButton(3);
    }
   
      println("enviamos mensaje");
  }
}

public void oscEvent(OscMessage theOscMessage) {
    /* print the address pattern and the typetag of the received OscMessage */
    int msg = theOscMessage.get(0).intValue();
    if(msg == 0){
      myPort.write(0);
    }
    
    if(msg == 1){
      myPort.write(9);
    }
    
     if(msg == 10){
      myPort.write(1);
      println("pintamos 10");
    }
    
    if(msg == 20){
      myPort.write(2);
      println("pintamos 20");
    }
    
    if(msg == 30){
      myPort.write(3);
      println("pintamos 30");
    }
      
  }

void keyPressed() {
  // Send the keystroke out:
  myPort.write(key-'0');
  whichKey = key-'0';
}