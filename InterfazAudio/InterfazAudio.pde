/**
 * Serial Duplex 
 * by Tom Igoe. 
 * 
 * Sends a byte out the serial port when you type a key
 * listens for bytes received, and displays their value. 
 * This is just a quick application for testing serial data
 * in both directions. 
 */


import processing.serial.*;
import oscP5.*;
import netP5.*;

import ddf.minim.*;
import ddf.minim.ugens.*;

final int MAX_SAMPLES = 8;
Minim minim;
AudioOutput out;
Sampler sampler[];

OscP5 oscP5;
NetAddress myRemoteLocation;

HashMap<String, Sampler> samples = new HashMap();

void setup() {
  size(400, 300);
  // create a font with the third font available to the system:
  PFont myFont = createFont(PFont.list()[2], 14);
  textFont(myFont);
  minim = new Minim(this);

  sampler = new Sampler[MAX_SAMPLES];

  out   = minim.getLineOut();

  loadRandomBank();

  oscP5 = new OscP5(this, 12004);
  myRemoteLocation = new NetAddress("127.0.0.1", 12000);
}

void loadRandomBank() {
  loadSamplesFromBank( (int)random(1, 8) );
}

void loadSamplesFromBank(int b) { // banks - 1 to 5
  b = constrain(b, 1, 8) * 10;
  for (int i = 0; i < MAX_SAMPLES; i++) {
    String num = nf(i + 1, 3);
    if (b >= 40) num = nf(i + 8, 3);
    String sFile = "Simon_" + b + "_M" + num + ".wav";
    println("Loading sample " + sFile);
    sampler[i] = samples.get(sFile);
    if (sampler[i] == null) {
      sampler[i]  = new Sampler( sFile, 4, minim );
      samples.put(sFile, sampler[i]);
    }
  }
  for (int i = 0; i < MAX_SAMPLES; i++) sampler[i].patch( out );
}

void doScaleUp() {
  println("Doing Scale Up");
  for (int i = 0; i < MAX_SAMPLES; i++) {
    sampler[i].trigger();
    delay(300);
  }
  delay(1000);
  sendButton(4);
}

void doScaleDown() {
  println("Doing Scale Down");
  for (int i = MAX_SAMPLES - 1; i >= 0; i--) {
    sampler[i].trigger();
    delay(300);
  }
  delay(1000);
  sendButton(5);
}

public void sendButton(int numB) {
  OscMessage myMessage = new OscMessage("/buttonPressed");

  myMessage.add(numB); /* add an int to the osc message */
  oscP5.send(myMessage, myRemoteLocation);
}

void draw() {
  background(0);
}

void keyPressed() {
  if (Character.toLowerCase(key) == 'r') loadRandomBank();
  if (Character.toLowerCase(key) == 'u') doScaleUp();
  if (Character.toLowerCase(key) == 'd') doScaleDown();
}


public void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  int msg = theOscMessage.get(0).intValue();
  if (msg == 0) {
    doScaleDown();
  }

  if (msg == 1) {
    loadRandomBank();   
    doScaleUp();
  }

  if (msg == 10) {
    println("pintamos 10");
    sampler[0].trigger();
  }

  if (msg == 20) {
    println("pintamos 20");
    sampler[3].trigger();
  }

  if (msg == 30) {
    println("pintamos 30");
    sampler[5].trigger();
  }
}