package org.mlp.cosa;

public class Pixels {

	Pixel[] pixels;

	public Pixels setColor(int color) {
		if (pixels != null)
			for (Pixel pixel : pixels) {
				pixel.setColor(color);
			}

		return this;
	}

	public Pixels setColorTarget(int color) {
		if (pixels != null)
			for (Pixel pixel : pixels) {
				pixel.setColorTarget(color);
			}

		return this;
	}

	public Pixels setState(int state) {
		if (pixels != null)
			for (Pixel pixel : pixels) {
				pixel.setState(state);
			}

		return this;
	}
}
