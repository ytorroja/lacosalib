package org.mlp.cosa;

public class CosaTest {

	public static void main(String[] args) {
		
		Cosa cosa = new Cosa();
		cosa.init(null, null);
		//color a toda la cosa
		cosa.setColor(0xFFFFFF);
		//color a una planta
		cosa.getByTag("1").setColor(0xFFFFFF);
		
		//los paneles de toda la cosa
		Panels allPanels = cosa.getPanels();
		
		for(Panel panel:allPanels){
			panel.setColor(0xFFFFFF);
		}
		
		//los paneles de una planta
		Panels panelsFloor = cosa.getByTag("2");
		
		for(Panel panel:panelsFloor){
			panel.setColor(0xFFFFFF);
		}
		
		//Un panel de un piso
		Panel panel0Floor2 = cosa.getByTag("2").get(0);
		panel0Floor2.setColor(0xFF00FF);
		//lo mismo 
		cosa.getByTag("2").get(0).setColor(0xFF00FF);
		
		//de un panel sus celdas
		panel0Floor2.setColor(0xFF00FF);
		panel0Floor2.getCells().get(0).setColor(0xFF00FF);
		
		//de una celda
		Cell cell = panel0Floor2.getCells().get(0);
		cell.setColor(0xff00ff);
		cell.getPixels()[0].setColor(0xff00ff);
		
	}
}
