package org.mlp.cosa;

import java.util.ArrayList;
import java.util.List;

public class Cell extends CosaStuff{

	Pixels pixels = new Pixels();
	
	public List<Point> polygon = new ArrayList<Point>();
	
	public Cell setColor(int c) {
		pixels.setColor(c);
		this.color = c;
		return this;
	}
	
	public Cell setColorTarget(int color) {
		this.colorTarget = color;
		pixels.setColorTarget(color);
		return this;
	}

	public Cell setState(int state) {
		pixels.setState(state);
		setState(state);
		return this;
	}

	
	public Pixel[] getPixels(){
		return pixels.pixels;
	}
}
