package org.mlp.cosa;

import java.util.Random;

public class Panel extends CosaStuff {

	Pixels pixels;

	Cells cells = new Cells();

	public Panel(String _name, int _x, int _y, int _w, int _h) {
		x = _x;
		y = _y;
		width = _w;
		height = _h;
		name = _name;
		Random random = new Random();
		color = random.nextInt(Integer.MAX_VALUE) | 0xFF000000;
		light = true;
		isDraw = false;
		debug = false;
	}

	public Panel setColor(int color) {
		super.setColor(color);
		if (cells != null)
			cells.setColor(color);
		return this;
	}

	public Panel setColorTarget(int color) {
		super.setColorTarget(color);
		cells.setColorTarget(color);
		return this;
	}

	public Cells getCells() {
		if (cells == null)
			return null;
		return cells;
	}

}
