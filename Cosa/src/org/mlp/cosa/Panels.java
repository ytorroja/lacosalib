package org.mlp.cosa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Panels extends ArrayList<Panel> {

	public Panels setColor(int color) {
		for (Panel panel : this) {
			panel.setColor(color);
		}
		return this;
	}

	public Panels setColorRandom() {
		Random r = new Random();
		for (Panel panel : this) {
			panel.setColor(r.nextInt(Integer.MAX_VALUE));
		}
		return this;
	}

	public Panels setColor(int r, int g, int b) {
		return this;
	}

	public Panels getByTag(String tag) {

		Panels panels = new Panels();

		for (Panel p : this) {
			if (p.tags.contains(tag)) {
				panels.add(p);
			}
		}

		return panels;
	}

	public Panels getByTag(Collection<String> tagsFilter) {

		Panels panels = new Panels();

		for (Panel p : this) {
			if (p.tags.contains(tagsFilter)) {
				panels.add(p);
			}
		}

		return panels;
	}

	public void setState(int newState) {
		for (Panel p : this) {
			p.state = newState;
		}
	}
}
