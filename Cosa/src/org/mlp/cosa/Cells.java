package org.mlp.cosa;

import java.util.ArrayList;
import java.util.List;

public class Cells extends ArrayList<Cell>{

	public Cells setColor(int color) {
		for (Cell cell : this) {
			cell.setColor(color);
		}
		return this;
	}
	
	public Cells setColorTarget(int color) {
		for (Cell cell : this) {
			cell.setColorTarget(color);
		}
		return this;
	}
	
	public Cells setState(int state) {
		for (Cell cell : this) {
			cell.setState(state);
		}
		return this;
	}

	public Cells setColor(int r, int g, int b) {
		return this;
	}

}
