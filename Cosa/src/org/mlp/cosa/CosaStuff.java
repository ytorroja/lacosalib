package org.mlp.cosa;

import java.util.List;

public abstract class CosaStuff {

	public int x, y;
	public int width, height;

	public boolean debug = false;

	public int color = 0xFFFFFF;

	public int state = 0;
	public int colorTarget = 0;
	
	public String name;
	
	public boolean isDraw = false;
	public boolean light = true;
	
	public List<String> tags = null;

	public CosaStuff setColor(int color) {
		this.color = color;
		return this;
	}

	public CosaStuff setState(int state) {
		this.state = state;
		return this;
	}
//
//	public CosaStuff setColor(int r, int g, int b) {
//		int rgb = r;
//		rgb = (rgb << 8) + g;
//		rgb = (rgb << 8) + b;
//
//		return this;
//	}

	public CosaStuff setColorTarget(int color) {
		this.colorTarget = color;
		return this;
	}
}
