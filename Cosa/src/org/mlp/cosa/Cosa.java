package org.mlp.cosa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Cosa {

	Panels panels;
	public Cells cells;
	Panels[] panelsByFloor;

	Map<String, Panels> panelsByTags = new HashMap<String, Panels>();
	Map<String, Cells> cellsByTags = new HashMap<String, Cells>();

	public static String FLOOR_1 = "floor1";
	public static String FLOOR_2 = "floor2";
	public static String FLOOR_3 = "floor3";

	public static String WALL = "wall";
	public static String CEIL = "ceil";

	public void init(File file, File fileCells) {
		// CARRGAMOS DE FICHERO

		if (file == null) {
			String fileName = "/Users/eduardomorianadelgado/git/lacosalib/Cosa/paneles.txt";
			file = new File(fileName);
		}

		if (fileCells == null) {
			String fileName = "/Users/eduardomorianadelgado/git/lacosalib/Cosa/CoordCeldas.txt";
			fileCells = new File(fileName);
		}
		// read file into stream, try-with-resources
		// Construct BufferedReader from FileReader
		initPanels(file);
		initCells(fileCells);

		for (Panel panel : panels) {
			String name = panel.name;
			name = name.replaceFirst("P", "C");
			for (Cell cell : cells) {
				if (panel.cells != null && cell.name.startsWith(name)) {
					panel.cells.add(cell);
				}
			}
		}
	}

	private void initPanels(File file) {
		BufferedReader br = null;
		panels = new Panels();
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			try {
				while ((line = br.readLine()) != null) {
					System.out.println(line);

					String parts[] = line.split("\\.");
					String name = parts[0];
					int x = Integer.parseInt(parts[1]);
					int y = Integer.parseInt(parts[2]);
					int w = Integer.parseInt(parts[3]);
					int h = Integer.parseInt(parts[4]);

					Panel panel = new Panel(name, x, y, w, h);
					panels.add(panel);

					List<String> tags = new ArrayList<String>();

					for (int j = 5; j < parts.length; j++) {
						String tag = parts[j];

						Panels panelByTag = panelsByTags.get(tag);
						if (panelByTag == null) {
							panelByTag = new Panels();
							panelsByTags.put(tag, panelByTag);
							tags.add(tag);
						}

						panelByTag.add(panel);

					}

					panel.tags = tags;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initCells(File file) {
		BufferedReader br = null;
		cells = new Cells();
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			try {
				while ((line = br.readLine()) != null) {
					System.out.println(line);

					String parts[] = line.split("\\.");
					String name = parts[0];

					Cell cell = new Cell();
					cell.name = name;
					for (int j = 1; j < parts.length; j += 2)
						cell.polygon.add(new Point(Integer.parseInt(parts[j]),
								Integer.parseInt(parts[j + 1])));
					cells.add(cell);

					List<String> tags = new ArrayList<String>();

					for (int j = 5; j < parts.length; j++) {
						String tag = parts[j];

						Cells cellsByTag = cellsByTags.get(tag);
						if (cellsByTag == null) {
							cellsByTag = new Cells();
							cellsByTags.put(tag, cellsByTag);
							tags.add(tag);
						}
						cellsByTag.add(cell);
					}

					cell.tags = tags;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Panels getByTag(String tag) {
		return panelsByTags.get(tag);
	}

	public Cosa setColor(int color) {
		panels.setColor(color);
		return this;
	}

	public Cosa setColorRandom() {
		// panels.setColor(color);
		return this;
	}

	public Cosa setColor(int r, int g, int b) {
		return this;
	}

	public Panels getPanels() {
		return panels;
	}

	public Cosa setState(int s) {
		panels.setState(s);
		return this;
	}

	public void setIsDraw(boolean b) {
		Panels panels = getPanels();
		for (Panel panel : panels) {
			for (Cell c : panel.getCells()) {
				c.isDraw = b;
			}
		}
	}
}
